---
layout:       post
title:        "Minecraft Legends"
date:         2023-04-15 16:00:00 +1000
author:       "Kitarei"
categories:   
tags:         

# POSTS LIST
class:       "style5"                         # config bg-color to post list card (1..6)
list-image:  "/assets/images/minecraftlegendr.jpg"       # config image to post list card (1..6)
description: >                                # config description to post list card
  Minecraft series spin-off blending
  survival and RTS together.

# POST HEADER
header-image: "/assets/images/minecraftlegends-header.jpg"      # config image to post header
alt-image:    "minecraft legends heading image" # config image description to alt att.
---

### What is it?
Minecraft Legends is the latest spin-off from the popular franchise, mooshing together the traditional survival-gather-craft elements of Minecraft with a new RTS spin. During my few days playing an early access version of the game I focused my time on progressing the campaign, but there is also a huge PvP aspect as well which I'll have to try out in the future.

Playing Legends feels like a fresh new experience, and it mixes resource gathering and real-time strategy expertly. The cutscenes are all fully voice acted and allows us to dive deeper into Minecraft's lore than ever before.

The tutorial is super quick and **definitely required** to teach us the ins and outs of gathering, building structures, and summoning our minions. My only real complaint is that I was unable to work out how to send only a *single* specific unit type to attack enemy units/structures. The tutorial introduces us to "banner view" which allows us to use some more complex commands such as asking our minions to focus on a particular target, and on the left-hand side there is an indicator stating that we're ordering *all mobs*, but I couldn't work out how to actually CHANGE that from *all mobs* to *ranged only* or *melee only*.

<center><img src="/assets/images/minecraftlegends/bannerview.png"></center>

Ideally I'd have loved to send melee mobs to focus on structures, and ranged mobs to focus on piglins! The banner menu certainly implies this is possible, perhaps I missed it in the tutorial or it wasn't outlined clearly enough for my 2 brain cells.

### Where is it?
Minecraft Legends will be available on [Game Pass][minecraftlegends-xgp] for both PC and Console. You can also pick it up on Steam, Nintendo Switch, and PlayStation. The game officially launches on April 18th at 9AM PDT.
<center><div class="box alt">
  <div class="row uniform">
    <div class="12u$"><span class="image fit"><img src="/assets/images/minecraftlegends/releasetimes.png" alt="map of minecraft legends release date times"></span></div>
  </div>
</div></center>

### Let's play?
Not exactly, but check out my quick 2 minute video review of my experience with the early access!

<iframe width="560" height="315" src="https://www.youtube.com/embed/pOqR49MLQeE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe> 

### What does it look like?
<div class="box alt">
  <div class="row uniform">
    <div class="6u"><span class="image fit"><img src="/assets/images/minecraftlegends/img01.png" alt=""></span></div>
    <div class="6u$"><span class="image fit"><img src="/assets/images/minecraftlegends/img03.jpg" alt=""></span></div>
    <div class="6u"><span class="image fit"><img src="/assets/images/minecraftlegends/img04.png" alt=""></span></div>
    <div class="6u$"><span class="image fit"><img src="/assets/images/minecraftlegends/img02.png" alt=""></span></div>
  </div>
</div>


[minecraftlegends-xgp]: https://www.xbox.com/en-AU/games/minecraft-legends
