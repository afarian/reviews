---
layout:       post
title:        "Turnip Boy Commits Tax Evation"
date:         2023-01-13 15:00:00 +1000
author:       "Kitarei"
categories:   
tags:         

# POSTS LIST
class:       "style2"                         # config bg-color to post list card (1..6)
list-image:  "/assets/images/turnip.jpg"       # config image to post list card (1..6)
description: >                                # config description to post list card
  Adventure, puzzles, and tax evasion
  meet adorable turnipy menace to society.

# POST HEADER
header-image: "/assets/images/turnip-header.jpg"      # config image to post header
alt-image:    "turnip boy header image" # config image description to alt att.
---

### What is it?
You play as Turnip Boy, a little shit of a turnip who's main goal in life is to evade taxes and be a general menace to society. The games is relatively short and can be completed in a few hours.

It's a little bit ridiculous and a lot hilarious - the humour is a tad messed up and the gameplay is old-school Zelda style. The story itself is simple, but the dialogue carries the game with it's incessant need to crack joke after joke.

I ended up completing all the achievements and getting 1000/1000 gamer score on Xbox, so I guess you could say I liked it a lot.

### Where is it?
Turnip Boy Commits Tax Evasion is available on Xbox [Game Pass][turnip-xgp] for both PC and Console. You can also pick it up on Nintendo Switch and PlayStation.

### Let's play?
Soz fam, I played this on the couch while I was sick with COVID, so no Let's Play video :(

Here's the trailer though:
<iframe width="560" height="315" src="https://www.youtube.com/embed/dBt-1ObT0NQ?controls=0" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe> 

### What does it look like?
It looks like <i>fucking mayhem</i>.
<div class="box alt">
  <div class="row uniform">
    <div class="12u$"><span class="image fit"><img src="/assets/images/tbcte/img03.jpg" alt=""></span></div>
    <div class="12u$"><span class="image fit"><img src="/assets/images/tbcte/img04.jpg" alt=""></span></div>
    <div class="12u$"><span class="image fit"><img src="/assets/images/tbcte/img05.jpg" alt=""></span></div>
  </div>
</div>


[turnip-xgp]: https://www.xbox.com/en-au/games/store/turnip-boy-commits-tax-evasion/9n0t8v0r7mbc
[tbrb-xgp]: https://www.xbox.com/en-au/games/store/turnip-boy-robs-a-bank/9ndt1ms39fxz
