---
layout:       post
title:        "Townscaper"
date:         2023-01-22 17:00:00 +1000
author:       "Kitarei"
categories:   
tags:         

# POSTS LIST
class:       "style3"                         # config bg-color to post list card (1..6)
list-image:  "/assets/images/townscaper.jpg"       # config image to post list card (1..6)
description: >                                # config description to post list card
  Adventure, puzzles, and tax evasion
  meet adorable turnipy menace to society.

# POST HEADER
header-image: "/assets/images/townscaper-header.jpg"      # config image to post header
alt-image:    "townscaper heading image" # config image description to alt att.
---

### What is it?
A relaxing town builder, not so much a game but more virtual legos. There is no story, no goals, no levels, you're not required to do <i>anything</i>.

You simply place pieces and watch your town over the water come to life.

To be completely honest, I played this game specifically because it's a <b>VERY EASY</b> 1000/1000 gamerscore on XBOX - like, no shit, it takes 10 minutes. I don't think I'd revisit this game again.

### Where is it?
Townscaper is available on Xbox [Game Pass][townscaper-xgp] for both PC and Console. You can also pick it up on Nintendo Switch, Steam, Epic Games, iOS, and Android. It doesn't look like it's available on PlayStation at the time of writing this.

### Let's play?
I didn't record shit - I spent 10 minutes on this game getting my gamer score and bouncing.

Here's the trailer though:
<iframe width="560" height="315" src="https://www.youtube.com/embed/hqq25n6cQqo" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture; web-share" allowfullscreen></iframe> 

### What does it look like?
<div class="box alt">
  <div class="row uniform">
    <div class="12u$"><span class="image fit"><img src="/assets/images/townscaper/townscaper01.jpg" alt=""></span></div>
    <div class="12u$"><span class="image fit"><img src="/assets/images/townscaper/townscaper02.jpg" alt=""></span></div>
    <div class="12u$"><span class="image fit"><img src="/assets/images/townscaper/townscaper03.jpg" alt=""></span></div>
  </div>
</div>


[townscaper-xgp]: https://www.xbox.com/en-AU/games/store/townscaper/9NFM39PSFXJD
