---
layout:       post
title:        "Pentiment"
date:         2022-11-15 15:00:00 +1000
author:       "Kitarei"
categories:   
tags:         

# POSTS LIST
class:       "style1"                         # config bg-color to post list card (1..6)
list-image:  "/assets/images/pentiment.jpg"       # config image to post list card (1..6)
description: >                                # config description to post list card
  An adventure video game by Obsidian Entertainment
  and published by Xbox Game Studios.

# POST HEADER
header-image: "/assets/images/pentiment-header.jpg"      # config image to post header
alt-image:    "pentiment header image" # config image description to alt att.
---
THIS GAME IS SO FUCKING GOOD HOLY SHIT.

### What is it?
Pentiment is a mystery adventure game set in ye olde Europe. You play as a guy called Andreas Maler, an artist who gets caught up in a series of stabby stabby murders at Kiersau Abbey. My Andreas is a horn-bag, but that's totally optional - you can play a less-crass version of Andreas pending you have the self-control to avoid the more vulgar options when setting up your background story (something I failed to do).

The selections you make in-game have an impact on the town of Tassing's future, and your choices have consequences. Honestly, I felt like I made all the wrong decisions during my play-through, some of the choices I was forced to make felt lose-lose, especially at the end of the first act.

### Where is it?
Pentiment is available from day one on [Game Pass][pentiment-xgp] for both PC and Console - November 15th 2022.

### Let's play?
Sure - why the fuck not.
<iframe width="560" height="315" src="https://www.youtube.com/embed/videoseries?list=PLFa422AdIUkXbSZhe2DJqat8vy7ZzhLGT" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe> 

### What does it look like?
These screenshots have absolutely zero fucking context, enjoy.
<div class="box alt">
  <div class="row uniform">
    <div class="12u$"><span class="image fit"><img src="/assets/images/pentiment/img03.png" alt=""></span></div>
    <div class="4u"><span class="image fit"><img src="/assets/images/pentiment/img01.png" alt=""></span></div>
    <div class="4u"><span class="image fit"><img src="/assets/images/pentiment/img02.png" alt=""></span></div>
    <div class="4u$"><span class="image fit"><img src="/assets/images/pentiment/img05.png" alt=""></span></div>
    <div class="12u$"><span class="image fit"><img src="/assets/images/pentiment/img04.png" alt=""></span></div>
    <div class="12u$"><span class="image fit"><img src="/assets/images/pentiment/img06.png" alt=""></span></div>
  </div>
</div>


[pentiment-xgp]: https://www.xbox.com/en-AU/games/pentiment
